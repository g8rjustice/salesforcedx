/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.project;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ProjectState;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import salesforce.dx.plugin.project.sfdx.SfdxProject;

/**
 *
 * @author seanjustice
 */
public class DxProject implements Project{

    private static final String PROJECT_FILE = "sfdx-project.json";

    public static boolean isDxProject(Project project){
        FileObject sfdxFileObj = project.getProjectDirectory().getFileObject(PROJECT_FILE);
        return sfdxFileObj != null;
    }
    
    private final FileObject projectDir;
    private final ProjectState state;
    private Lookup lookup = null;
    
    public DxProject(FileObject dir, ProjectState state){
        this.projectDir = dir;
        this.state = state;
    }
    
    @Override
    public FileObject getProjectDirectory() {
        return projectDir;
    }

    @Override
    public Lookup getLookup() {
        if(lookup == null){
            lookup = Lookups.fixed(new Object [] {
                new DxProjectInformation(this),
                new DxProjectLogicalView(this),
                this
            });
        }
        return lookup;
    }
    
    
    public SfdxProject getSfdxProject(){
        
        try{
            FileObject sfdxFileObj = projectDir.getFileObject(PROJECT_FILE);
            File sfdxFile = FileUtil.toFile(sfdxFileObj);
            SfdxProject sfdxProject = new ObjectMapper().readValue(sfdxFile, SfdxProject.class);
            return sfdxProject;
            
        }catch(IOException ex){
            return null;
        }
    }

    
}
