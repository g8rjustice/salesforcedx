/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.project.wizard;

/**
 *
 * @author seanjustice
 */
public enum ApexType {
    
    CLASS("class"){
        
        @Override
        public void validate(String name) throws ApexTypeException{
            validateName(name);
        }
    },
    EXCEPTION("exception"){
        
        @Override
        public void validate(String name) throws ApexTypeException{
            validateName(name);
            
            if(name.endsWith("Exception") == false){
                throw new ApexTypeException("Class name must end with Exception");
            }
        }
    },
    UNIT_TEST("unit test"){
        
        @Override
        public void validate(String name) throws ApexTypeException{
            validateName(name);
            
            if(name.endsWith("Test") == false){
                throw new ApexTypeException("Class name must end with Test");
            }
        }
    },
    TRIGGER("trigger"){
        
        @Override
        public void validate(String name) throws ApexTypeException{
            validateName(name);
        }
    },
    LIGHTNING("lightning"){
        
        @Override
        public void validate(String name) throws ApexTypeException{
            validateName(name);
        }
    },
    INBOUND_EMAIL_SERVICE("inbound email service"){
        
        @Override
        public void validate(String name) throws ApexTypeException{
            validateName(name);
        }
    };
    
    private final String type;
    
    ApexType(String name){
        this.type = name;
    }
    
    public String getType(){
        return type;
    }
    
    public abstract void validate(String name) throws ApexTypeException;
    
    protected void validateName(String name) throws ApexTypeException{
            if(name.equals("")){
                throw new ApexTypeException("Name field can not be empty");
            }
            
            if(name.length() > 40){
                throw new ApexTypeException("Name can not be over 40 characters");
            }
            
            char start = name.charAt(0);
            if(Character.isLetter(start) == false){
                throw new ApexTypeException("Name must start with a letter");
            }
    }
    
    public static ApexType get(String t){
        
        for(ApexType act : ApexType.values()){
            if(act.type.equals(t)){
                return act;
            }
        }
        
        throw new IllegalArgumentException(t + " is not a valid apex class type");
    }
}
