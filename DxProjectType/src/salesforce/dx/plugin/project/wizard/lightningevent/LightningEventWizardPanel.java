/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.project.wizard.lightningevent;

import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.HelpCtx;
import salesforce.dx.plugin.project.wizard.ApexType;
import salesforce.dx.plugin.project.wizard.ApexTypeException;
import salesforce.dx.plugin.project.wizard.lightning.LightningVisualPanel;
import salesforce.dx.plugin.project.wizard.location.ProjectPackageLocationSetter;
import salesforce.dx.plugin.project.wizard.location.ProjectPackageLocator;

public class LightningEventWizardPanel implements WizardDescriptor.ValidatingPanel<WizardDescriptor> {

    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private LightningVisualPanel component;

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    @Override
    public LightningVisualPanel getComponent() {
        if (component == null) {
            component = new LightningVisualPanel();
        }
        return component;
    }

    public ProjectPackageLocator getLocator(){
        return getComponent();
    }

    @Override
    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx("help.key.here");
    }

    @Override
    public boolean isValid() {
        // If it is always OK to press Next or Finish, then:
        return true;
        // If it depends on some condition (form filled out...) and
        // this condition changes (last form field filled in...) then
        // use ChangeSupport to implement add/removeChangeListener below.
        // WizardDescriptor.ERROR/WARNING/INFORMATION_MESSAGE will also be useful.
    }

    @Override
    public void addChangeListener(ChangeListener l) {
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
    }

    @Override
    public void readSettings(WizardDescriptor wiz) {
        ProjectPackageLocationSetter.set(wiz, getLocator());
        // use wiz.getProperty to retrieve previous panel state
    }

    @Override
    public void storeSettings(WizardDescriptor wiz) {
        LightningVisualPanel componentPanel = getComponent();
        wiz.putProperty("lightningName", componentPanel.getLightningName());
        // use wiz.putProperty to remember current panel state
    }

    @Override
    public void validate() throws WizardValidationException {
        String name = getComponent().getLightningName();

        try{
            ApexType.LIGHTNING.validate(name);
        }catch(ApexTypeException ex){
            throw new WizardValidationException(null, ex.getMessage(), null);
        }
        
    }
}
