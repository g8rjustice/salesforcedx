/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.project.wizard;

/**
 *
 * @author seanjustice
 */
public class ApexTypeException extends Exception {

    /**
     * Creates a new instance of <code>ApexClassTypeException</code> without
     * detail message.
     */
    public ApexTypeException() {
    }

    /**
     * Constructs an instance of <code>ApexClassTypeException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ApexTypeException(String msg) {
        super(msg);
    }
}
