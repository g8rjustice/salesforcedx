/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.project.wizard.newproject;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.templates.TemplateRegistration;
import org.netbeans.spi.project.ui.support.ProjectChooser;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle.Messages;
import salesforce.dx.plugin.service.creator.CreatorException;
import salesforce.dx.plugin.service.creator.project.DxProjectCreator;
import salesforce.dx.plugin.sfdx.service.SfdxService;

// TODO define position attribute
@TemplateRegistration(
        folder = "Project/Salesforce", 
        displayName = "#NewDxProjectWizardIterator_displayName", 
        iconBase = "salesforce/dx/plugin/project/resources/DxProjectIcon.png", 
        description = "newDxProject.html")
@Messages("NewDxProjectWizardIterator_displayName=Dx Project")
public final class NewDxProjectWizardIterator implements WizardDescriptor.InstantiatingIterator<WizardDescriptor> {

    public final static String PROJECT_DIR = "projdir";
    public final static String PROJECT_NAME = "name";
    public final static String PACKAGE_DIR = "package-directory";
    public final static String NAMESPACE = "namespace";
    public final static String CREATED_FOLDER = "createdFolder";
    
    private int index;
    private WizardDescriptor.Panel[] panels;
    private WizardDescriptor wiz;

    public NewDxProjectWizardIterator() {
    }

    private WizardDescriptor.Panel[] createPanels() {
        return new WizardDescriptor.Panel[]{
            new NewDxProjectWizardLocationPanel(),
            new NewDxProjectWizardSettingsPanel()
        };
    }

    private String[] createSteps() {
        return new String[]{
            "Name and Location",
            "DX Settings"
        };
    }

    @Override
    public Set<?> instantiate() throws IOException {

        File projectDir = FileUtil.normalizeFile((File)wiz.getProperty(PROJECT_DIR));
        Path path = projectDir.getParentFile().toPath();
        
        try{
            String name = (String)wiz.getProperty(PROJECT_NAME);
            String packageDirectory = (String)wiz.getProperty(PACKAGE_DIR);
            String namespace = (String)wiz.getProperty(NAMESPACE);
            if(namespace.isEmpty()){
                namespace = null;
            }
            
            DxProjectCreator creator = SfdxService.<DxProjectCreator>get(DxProjectCreator.class);
            creator.create(path, name, packageDirectory, namespace);
        }catch(CreatorException ex){
            System.err.println(ex.getMessage());
        }
        
        File parent = projectDir.getParentFile();
        if((parent != null) && (parent.exists())){
            ProjectChooser.setProjectsFolder(projectDir);
        }
        
        FileObject projectDirObj = FileUtil.toFileObject(projectDir);
        Set<FileObject> results = new HashSet<>();
        results.add(projectDirObj);
        // TODO return set of FileObject (or DataObject) you have created
        return results;
    }

    @Override
    public void initialize(WizardDescriptor wiz) {
        this.wiz = wiz;
        index = 0;
        panels = createPanels();
        // Make sure list of steps is accurate.
        String[] steps = createSteps();
        for (int i = 0; i < panels.length; i++) {
            Component c = panels[i].getComponent();
            if (steps[i] == null) {
                // Default step name to component name of panel.
                // Mainly useful for getting the name of the target
                // chooser to appear in the list of steps.
                steps[i] = c.getName();
            }
            if (c instanceof JComponent) { // assume Swing components
                JComponent jc = (JComponent) c;
                // Step #.
                // TODO if using org.openide.dialogs >= 7.8, can use WizardDescriptor.PROP_*:
                jc.putClientProperty("WizardPanel_contentSelectedIndex", new Integer(i));
                // Step name (actually the whole list for reference).
                jc.putClientProperty("WizardPanel_contentData", steps);
            }
        }
    }


    @Override
    public void uninitialize(WizardDescriptor wiz) {
        this.wiz.putProperty(NewDxProjectWizardIterator.PROJECT_DIR, null);
        this.wiz.putProperty(NewDxProjectWizardIterator.PROJECT_NAME, null);
        this.wiz = null;
        panels = null;
    }

    @Override
    public String name() {
        return MessageFormat.format("{0} of {1}",
                new Object[]{new Integer(index + 1), new Integer(panels.length)});
    }

    @Override
    public boolean hasNext() {
        return index < panels.length - 1;
    }

    @Override
    public boolean hasPrevious() {
        return index > 0;
    }

    @Override
    public void nextPanel() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        index++;
    }

    @Override
    public void previousPanel() {
        if (!hasPrevious()) {
            throw new NoSuchElementException();
        }
        index--;
    }

    @Override
    public WizardDescriptor.Panel current() {
        return panels[index];
    }

    // If nothing unusual changes in the middle of the wizard, simply:
    @Override
    public void addChangeListener(ChangeListener l) {
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
    }

}
