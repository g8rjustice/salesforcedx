/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.project.wizard.location;

import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.openide.WizardDescriptor;
import salesforce.dx.plugin.project.DxProjectInformation;

/**
 *
 * @author seanjustice
 */
public class ProjectPackageLocationSetter {

    public static void set(WizardDescriptor wiz, ProjectPackageLocator locator){
        
        Project project = (Project)wiz.getProperty(ProjectPackageWizardPanel.PROJECT);
        String packageName = (String)wiz.getProperty(ProjectPackageWizardPanel.PACKAGE);
        
        ProjectInformation projectInfo = project.getLookup().lookup(DxProjectInformation.class);

        ProjectPackageLocationPanel panel = locator.getPanel();
        panel.setLocation(projectInfo.getDisplayName(), packageName);
        
    }
}
