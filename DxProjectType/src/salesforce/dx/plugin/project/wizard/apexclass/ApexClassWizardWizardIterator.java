/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.project.wizard.apexclass;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.api.templates.TemplateRegistration;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle.Messages;
import salesforce.dx.plugin.project.wizard.ApexType;
import salesforce.dx.plugin.project.wizard.location.ProjectPackageWizardPanel;
import salesforce.dx.plugin.service.creator.CreatorException;
import salesforce.dx.plugin.service.creator.apex.ApexClassCreator;
import salesforce.dx.plugin.sfdx.service.SfdxService;

@TemplateRegistration(
    folder="Salesforce",
    iconBase = "salesforce/dx/plugin/project/wizard/resources/outline_cloud_black_16pt_1x.png",
    displayName = "#ApexClassWizardWizardIterator_displayName",
    description = "ClsClassDescription.html"
)
@Messages("ApexClassWizardWizardIterator_displayName=Apex Class")
public final class ApexClassWizardWizardIterator implements WizardDescriptor.InstantiatingIterator<WizardDescriptor> {

    private static final Map<ApexType, ApexClassCreator.Type> types;
    
    static{
        types = new HashMap<>();
        types.put(ApexType.CLASS, ApexClassCreator.Type.CLASS);
        
    }
    private int index;

    private WizardDescriptor wizard;
    private List<WizardDescriptor.Panel<WizardDescriptor>> panels;

    private List<WizardDescriptor.Panel<WizardDescriptor>> getPanels() {
        if (panels == null) {
            panels = new ArrayList<WizardDescriptor.Panel<WizardDescriptor>>();
            panels.add(new ProjectPackageWizardPanel());
            panels.add(new ApexClassWizardWizardPanel());
            String[] steps = createSteps();
            for (int i = 0; i < panels.size(); i++) {
                Component c = panels.get(i).getComponent();
                if (steps[i] == null) {
                    // Default step name to component name of panel. Mainly
                    // useful for getting the name of the target chooser to
                    // appear in the list of steps.
                    steps[i] = c.getName();
                }
                if (c instanceof JComponent) { // assume Swing components
                    JComponent jc = (JComponent) c;
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_SELECTED_INDEX, i);
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DATA, steps);
                    jc.putClientProperty(WizardDescriptor.PROP_AUTO_WIZARD_STYLE, true);
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DISPLAYED, true);
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_NUMBERED, true);
                }
            }
        }
        return panels;
    }

    @Override
    public Set<?> instantiate() throws IOException {
        // TODO return set of FileObject (or DataObject) you have created
        Project project = (Project)wizard.getProperty(ProjectPackageWizardPanel.PROJECT);
        String packageName = (String)wizard.getProperty(ProjectPackageWizardPanel.PACKAGE);
        String className = (String)wizard.getProperty(ApexClassWizardWizardPanel.CLASS_NAME);
        ApexType apexType = (ApexType)wizard.getProperty(ApexClassWizardWizardPanel.CLASS_TYPE);
        
        FileObject fileObj = project.getProjectDirectory();
        Path projectPath = Paths.get(fileObj.toURI());
        Path classPath = projectPath.resolve(packageName).resolve("main");
        
        if(apexType == ApexType.UNIT_TEST){
            classPath = classPath.resolve("test");
        }else{
            classPath = classPath.resolve("default");
        }
        
        classPath = classPath.resolve("classes");
        
        
        try{
            ApexClassCreator creator = SfdxService.<ApexClassCreator>get(ApexClassCreator.class);
            creator.create(classPath, className, types.get(apexType));
            
            File classFile = classPath.resolve(className + ".cls").toFile();
            Set<FileObject> createdFiles = new HashSet<>();
            createdFiles.add(FileUtil.toFileObject(classFile));
            return createdFiles;
            
        }catch(CreatorException ex){
            System.err.println(ex.getMessage());
        }
        
        return Collections.emptySet();
    }

    @Override
    public void initialize(WizardDescriptor wizard) {
        this.wizard = wizard;
    }

    @Override
    public void uninitialize(WizardDescriptor wizard) {
        panels = null;

    }

    @Override
    public WizardDescriptor.Panel<WizardDescriptor> current() {
        return getPanels().get(index);
    }

    @Override
    public String name() {
        return index + 1 + ". from " + getPanels().size();
    }

    @Override
    public boolean hasNext() {
        return index < getPanels().size() - 1;
    }

    @Override
    public boolean hasPrevious() {
        return index > 0;
    }

    @Override
    public void nextPanel() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        index++;
    }

    @Override
    public void previousPanel() {
        if (!hasPrevious()) {
            throw new NoSuchElementException();
        }
        index--;
    }

    // If nothing unusual changes in the middle of the wizard, simply:
    @Override
    public void addChangeListener(ChangeListener l) {
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
    }
    // If something changes dynamically (besides moving between panels), e.g.
    // the number of panels changes in response to user input, then use
    // ChangeSupport to implement add/removeChangeListener and call fireChange
    // when needed

    // You could safely ignore this method. Is is here to keep steps which were
    // there before this wizard was instantiated. It should be better handled
    // by NetBeans Wizard API itself rather than needed to be implemented by a
    // client code.
    private String[] createSteps() {
        String[] beforeSteps = (String[]) wizard.getProperty("WizardPanel_contentData");
        assert beforeSteps != null : "This wizard may only be used embedded in the template wizard";
        String[] res = new String[(beforeSteps.length - 1) + panels.size()];
        for (int i = 0; i < res.length; i++) {
            if (i < (beforeSteps.length - 1)) {
                res[i] = beforeSteps[i];
            } else {
                res[i] = panels.get(i - beforeSteps.length + 1).getComponent().getName();
            }
        }
        return res;
    }

}
