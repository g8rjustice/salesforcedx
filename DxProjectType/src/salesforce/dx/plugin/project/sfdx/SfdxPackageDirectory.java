/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.project.sfdx;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author seanjustice
 */
public class SfdxPackageDirectory {
    public String path;
    @JsonProperty("default")
    public boolean _default;
    
}
