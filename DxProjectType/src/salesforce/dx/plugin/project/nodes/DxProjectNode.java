/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.project.nodes;

import java.awt.Image;
import javax.swing.Action;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.CommonProjectActions;
import org.netbeans.spi.project.ui.support.NodeFactorySupport;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

/**
 *
 * @author seanjustice
 */
public class DxProjectNode extends FilterNode{

    @StaticResource()
    public static final String DX_ICON = "salesforce/dx/plugin/project/resources/DxProjectIcon.png";

    private final Project project;
    
    public DxProjectNode(Node node, Project project) throws DataObjectNotFoundException{
        super(
            node,
            NodeFactorySupport.createCompositeChildren(
                    project, 
                    "Projects/salesforce-dx-plugin-project/Nodes"),
            new ProxyLookup(
                new Lookup[] {
                    Lookups.singleton(project),
                    node.getLookup()
                }
            )
        );
        
        this.project = project;
    }
    
    @Override
    public Action [] getActions(boolean context){
        return new Action [] {
            CommonProjectActions.newFileAction(),
            CommonProjectActions.copyProjectAction(),
            CommonProjectActions.deleteProjectAction(),
            CommonProjectActions.closeProjectAction()
        };
    }
    
    @Override
    public Image getIcon(int type){
        return ImageUtilities.loadImage(DX_ICON);
    }
    
    @Override
    public Image getOpenedIcon(int type){
        return getIcon(type);
    }
    
    @Override
    public String getDisplayName(){
        return project.getProjectDirectory().getName();
    }
    
}
