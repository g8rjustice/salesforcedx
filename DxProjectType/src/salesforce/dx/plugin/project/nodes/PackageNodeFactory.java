/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.project.nodes;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeList;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import salesforce.dx.plugin.project.sfdx.SfdxPackageDirectory;
import salesforce.dx.plugin.project.sfdx.SfdxProject;

/**
 *
 * @author seanjustice
 */
@NodeFactory.Registration(projectType = "salesforce-dx-plugin-project", position = 10)
public class PackageNodeFactory implements NodeFactory{

    @Override
    public NodeList<?> createNodes(Project project) {
        return new SourceNodeList(project);
    }
    
    private class SourceNodeList implements NodeList<Node>{

        private static final String PROJECT_FILE = "sfdx-project.json";
        
        private final Project project;
        
        public SourceNodeList(Project project){
            this.project = project;
        }
        
        @Override
        public List<Node> keys() {
            
            List<Node> result = new ArrayList<>();
            
            try {
                FileObject sfdxFileObj = project.getProjectDirectory().getFileObject(PROJECT_FILE);
                File sfdxFile = FileUtil.toFile(sfdxFileObj);
                SfdxProject sfdxProject = new ObjectMapper().readValue(sfdxFile, SfdxProject.class);
                
                for(SfdxPackageDirectory packageDirectory : sfdxProject.packageDirectories){
                    FileObject dir = project.getProjectDirectory().getFileObject(packageDirectory.path);
                    
                    if(dir != null){
                        result.add(DataObject.find(dir).getNodeDelegate());

                    }
                }
                
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }finally{
                return result;
            }
        }

        @Override
        public void addChangeListener(ChangeListener cl) {

        }

        @Override
        public void removeChangeListener(ChangeListener cl) {

        }

        @Override
        public Node node(Node node) {
            return new FilterNode(node);
        }

        @Override
        public void addNotify() {

        }

        @Override
        public void removeNotify() {

        }
        
    }
}
