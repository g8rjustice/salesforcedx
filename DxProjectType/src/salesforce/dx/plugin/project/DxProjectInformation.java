/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.project;

import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.openide.util.ImageUtilities;

/**
 *
 * @author seanjustice
 */
public class DxProjectInformation implements ProjectInformation{

    @StaticResource()
    public static final String DX_ICON = "salesforce/dx/plugin/project/resources/DxProjectIcon.png";
    
    private final Project project;
    
    public DxProjectInformation(Project project){
        this.project = project;
    }
    
    @Override
    public String getName() {
        return project.getProjectDirectory().getName();
    }

    @Override
    public String getDisplayName() {
        return getName();
    }

    @Override
    public Icon getIcon() {
        return new ImageIcon(ImageUtilities.loadImage(DX_ICON));
    }

    @Override
    public Project getProject() {
        return project;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener pl) {
        
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener pl) {
        
    }
    
}
