/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.sfdx.service.create.apex;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import salesforce.dx.plugin.service.creator.CreatorException;
import salesforce.dx.plugin.service.creator.apex.ApexClassCreator;
import salesforce.dx.plugin.sfdx.service.runner.SfdxRunner;

/**
 *
 * @author seanjustice
 */
public class SfdxApexClassCreator implements ApexClassCreator{

    private final static Map<Type, String> types;
    
    static{
        types = new HashMap<>();
        types.put(Type.CLASS, "DefaultApexClass");
        types.put(Type.EXCEPTION, "ApexException");
        types.put(Type.UNIT_TEST, "ApexUnitTest");
        types.put(Type.INBOUND_EMAIL_SERVICE, "InboundEmailService");
    }
    
    private final SfdxRunner runner;
    
    public SfdxApexClassCreator(SfdxRunner runner){
        this.runner = runner;
    }
    
    @Override
    public void create(Path path, String name, Type type) throws CreatorException {
        
        
        String outputDir = path.toAbsolutePath().toFile().getAbsolutePath();
        
        List<String> args = new ArrayList<>();
        args.add("force:apex:class:create");
        args.add("-n");
        args.add(name);
        args.add("-t");
        args.add(types.get(type));
        args.add("-d");
        args.add(outputDir);
        
        try{
            
            if(Files.exists(path) == false){
                Files.createDirectories(path);
            }
            
            int result = runner.runner(args);

            if(result != 0){
                throw new CreatorException(String.format("Unable to run the command %d", result));
            }
        }catch(IOException | InterruptedException ex){
            throw new CreatorException(ex);
        }
    }
    
}
