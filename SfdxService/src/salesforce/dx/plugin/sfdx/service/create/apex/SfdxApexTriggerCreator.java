/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.sfdx.service.create.apex;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import salesforce.dx.plugin.service.creator.CreatorException;
import salesforce.dx.plugin.service.creator.apex.ApexTriggerCreator;
import salesforce.dx.plugin.sfdx.service.runner.SfdxRunner;

/**
 *
 * @author seanjustice
 */
public class SfdxApexTriggerCreator implements ApexTriggerCreator{

    private static final Map<ApexTriggerCreator.Events, String> eventMap = new EnumMap(ApexTriggerCreator.Events.class);
    
    static{
        eventMap.put(Events.AFTER_INSERT, "after insert");
        eventMap.put(Events.AFTER_DELETE, "after delete");
        eventMap.put(Events.AFTER_UNDELETE, "after undelete");
        eventMap.put(Events.AFTER_UPDATE, "after update");
        eventMap.put(Events.BEFORE_DELETE, "before delete");
        eventMap.put(Events.BEFORE_INSERT, "before insert");
        eventMap.put(Events.BEFORE_UPDATE, "before update");
    }
    
    private final SfdxRunner runner;
    
    public SfdxApexTriggerCreator(SfdxRunner runner){
        this.runner = runner;
    }
    
    @Override
    public void create(Path path, String name, String sObject, Set<ApexTriggerCreator.Events> events) throws CreatorException {
        
        String outputDir = path.toAbsolutePath().toFile().getAbsolutePath();
        String eventStr = createEventParams(events);
        List<String> args = new ArrayList<>();
        args.add("force:apex:trigger:create");
        args.add("-n");
        args.add(name);
        args.add("-e");
        args.add(eventStr);
        args.add("-s");
        args.add(sObject);
        args.add("-d");
        args.add(outputDir);
        
        try{
            
            if(Files.exists(path) == false){
                Files.createDirectories(path);
            }
            
            int result = runner.runner(args);

            if(result != 0){
                throw new CreatorException(String.format("Unable to run the command, create trigger, %d", result));
            }
        }catch(IOException | InterruptedException ex){
            throw new CreatorException(ex);
        }
    }
    
    private String createEventParams(Set<ApexTriggerCreator.Events> events){

        List<String> evts = new ArrayList<>();
        
        for(ApexTriggerCreator.Events key : events){
            evts.add(eventMap.get(key));
        }
        
        return String.join(", ", evts);
    }
}
