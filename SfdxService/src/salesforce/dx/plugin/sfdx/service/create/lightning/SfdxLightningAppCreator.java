/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.sfdx.service.create.lightning;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import salesforce.dx.plugin.service.creator.CreatorException;
import salesforce.dx.plugin.service.creator.lightning.LightningAppCreator;
import salesforce.dx.plugin.sfdx.service.runner.SfdxRunner;

/**
 *
 * @author seanjustice
 */
public class SfdxLightningAppCreator implements LightningAppCreator{

    private final SfdxRunner runner;
    
    public SfdxLightningAppCreator(SfdxRunner runner){
        this.runner = runner;
    }
    
    @Override
    public void create(Path path, String name) throws CreatorException {
        
        String outputDir = path.toAbsolutePath().toFile().getAbsolutePath();
        
        List<String> args = new ArrayList<>();
        args.add("force:lightning:app:create");
        args.add("-n");
        args.add(name);
        args.add("-d");
        args.add(outputDir);
        
        try{
            
            if(Files.exists(path) == false){
                Files.createDirectories(path);
            }
            
            int result = runner.runner(args);

            if(result != 0){
                throw new CreatorException(String.format("Unable to run the command %d", result));
            }
        }catch(IOException | InterruptedException ex){
            throw new CreatorException(ex);
        }
    }
    
}
