/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.sfdx.service.create.project;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import salesforce.dx.plugin.service.creator.CreatorException;
import salesforce.dx.plugin.service.creator.project.DxProjectCreator;
import salesforce.dx.plugin.sfdx.service.runner.SfdxRunner;

/**
 *
 * @author seanjustice
 */
public class SfdxProjectCreator implements DxProjectCreator{
    
    private final SfdxRunner runner;
    
    public SfdxProjectCreator(SfdxRunner runner){
        this.runner = runner;
    }
    
    @Override
    public void create(Path path, String name, String packageDirectory, String namespace) throws CreatorException {
        
        String outputDir = path.toAbsolutePath().toFile().getAbsolutePath();
        
        List<String> args = new ArrayList<>();
        args.add("force:project:create");
        args.add("-n");
        args.add(name);
        args.add("-d");
        args.add(outputDir);
        args.add("-p");
        args.add(packageDirectory);
        
        if(namespace != null){
            args.add("-s");
            args.add(namespace);
        }
        
        try{
            
            if(Files.exists(path) == false){
                Files.createDirectories(path);
            }
            
            int result = runner.runner(args);

            if(result != 0){
                throw new CreatorException(String.format("Unable to run the command [force:project:create] %d", result));
            }
        }catch(IOException | InterruptedException ex){
            throw new CreatorException(ex);
        }
        
    }
}
