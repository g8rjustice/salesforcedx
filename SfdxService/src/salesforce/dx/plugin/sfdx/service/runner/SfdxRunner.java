/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.sfdx.service.runner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingUtilities;
import org.netbeans.api.io.OutputWriter;
import org.openide.util.Exceptions;

/**
 *
 * @author seanjustice
 */
public class SfdxRunner {

    public static SfdxRunner newInstance(String executer, OutputWriter outputStream, OutputWriter errorStream){
        return new SfdxRunner(executer, outputStream, errorStream);
    }
    
    private final String executer;
    private final OutputWriter outputStream;
    private final OutputWriter errorStream;
    
    private SfdxRunner(String executer, OutputWriter outputStream, OutputWriter errorStream){
        this.executer = executer;
        this.outputStream = outputStream;
        this.errorStream = errorStream;
    }
    
    public int runner(List<String> args) throws IOException, InterruptedException{
    
        List<String> cmds = new ArrayList<>();
        cmds.add(executer);
        cmds.addAll(args);
        
        ProcessBuilder pb = new ProcessBuilder(cmds);
        Process process = pb.start();
        
        final InputStream is = process.getInputStream();

        Thread inputReader = new Thread(new Runnable(){
            @Override
            public void run(){
                BufferedReader inReader = new BufferedReader(new InputStreamReader(is));
                String line;
                try {
                    while((line = inReader.readLine()) != null){
                        
                        final String outputLine = line;
                        
                        SwingUtilities.invokeLater(new Runnable(){
                            @Override
                            public void run(){
                                outputStream.println(outputLine);
                            }
                        });
                    }
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        });
        inputReader.start();

        final InputStream es = process.getErrorStream();

        Thread errorReader = new Thread(new Runnable(){
            @Override
            public void run(){
                BufferedReader errReader = new BufferedReader(new InputStreamReader(es));
                String line;
                try {
                    while((line = errReader.readLine()) != null){
                        final String outputLine = line;
                        
                        SwingUtilities.invokeLater(new Runnable(){
                            @Override
                            public void run(){
                                errorStream.println(outputLine);
                            }
                        });
                    }
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        });
        errorReader.start();
        
        return process.waitFor();
        
    }
}
