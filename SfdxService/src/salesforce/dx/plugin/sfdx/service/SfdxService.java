/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.sfdx.service;

import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.io.IOProvider;
import org.netbeans.api.io.InputOutput;
import salesforce.dx.plugin.preferences.PreferencesService;
import salesforce.dx.plugin.preferences.SfdxPreferences;
import salesforce.dx.plugin.service.creator.apex.ApexClassCreator;
import salesforce.dx.plugin.service.creator.apex.ApexTriggerCreator;
import salesforce.dx.plugin.service.creator.lightning.LightningAppCreator;
import salesforce.dx.plugin.service.creator.lightning.LightningComponentCreator;
import salesforce.dx.plugin.service.creator.lightning.LightningEventCreator;
import salesforce.dx.plugin.service.creator.lightning.LightningInterfaceCreator;
import salesforce.dx.plugin.service.creator.lightning.LightningTestCreator;
import salesforce.dx.plugin.service.creator.project.DxProjectCreator;
import salesforce.dx.plugin.sfdx.service.create.apex.SfdxApexClassCreator;
import salesforce.dx.plugin.sfdx.service.create.apex.SfdxApexTriggerCreator;
import salesforce.dx.plugin.sfdx.service.create.lightning.SfdxLightningAppCreator;
import salesforce.dx.plugin.sfdx.service.create.lightning.SfdxLightningComponentCreator;
import salesforce.dx.plugin.sfdx.service.create.lightning.SfdxLightningEventCreator;
import salesforce.dx.plugin.sfdx.service.create.lightning.SfdxLightningInterfaceCreator;
import salesforce.dx.plugin.sfdx.service.create.lightning.SfdxLightningTestCreator;
import salesforce.dx.plugin.sfdx.service.create.project.SfdxProjectCreator;
import salesforce.dx.plugin.sfdx.service.runner.SfdxRunner;

/**
 *
 * @author seanjustice
 */
public class SfdxService {
 
    private static final Map<Class<?>, Object> services;
    
    static{
        InputOutput io = IOProvider.getDefault().getIO("Salesforce", true);
        SfdxPreferences sfdxPreferences = PreferencesService.getSfdxPreferences();
        SfdxRunner sfdxRunner = SfdxRunner.newInstance(
                sfdxPreferences.getCliPath(), 
                io.getOut(), 
                io.getErr());
        
        services = new HashMap<>();
        services.put(ApexClassCreator.class, new SfdxApexClassCreator(sfdxRunner));
        services.put(DxProjectCreator.class, new SfdxProjectCreator(sfdxRunner));
        services.put(ApexTriggerCreator.class, new SfdxApexTriggerCreator(sfdxRunner));
        services.put(LightningComponentCreator.class, new SfdxLightningComponentCreator(sfdxRunner));
        services.put(LightningAppCreator.class, new SfdxLightningAppCreator(sfdxRunner));
        services.put(LightningEventCreator.class, new SfdxLightningEventCreator(sfdxRunner));
        services.put(LightningInterfaceCreator.class, new SfdxLightningInterfaceCreator(sfdxRunner));
        services.put(LightningTestCreator.class, new SfdxLightningTestCreator(sfdxRunner));
    }
    
    public static <T> T get(Class<T> clazz){
        Object service = services.get(clazz);
        return clazz.cast(service);
    }
}
