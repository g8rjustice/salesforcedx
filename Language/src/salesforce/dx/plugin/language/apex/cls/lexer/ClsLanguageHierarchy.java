/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.language.apex.cls.lexer;

import salesforce.dx.plugin.language.apex.lexar.ApexLanguageHierarchy;

/**
 *
 * @author seanjustice
 */
public class ClsLanguageHierarchy extends ApexLanguageHierarchy{

    @Override
    protected String mimeType() {
        return "text/x-cls";
    }
    
}
