/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.language.apex.lexar;

import org.netbeans.api.lexer.TokenId;

/**
 *
 * @author seanjustice
 */
public class ApexTokenId implements TokenId{

    private final String name;
    private final String primaryCategory;
    private final int id;

    public ApexTokenId(
            String name,
            String primaryCategory,
            int id) {
        this.name = name;
        this.primaryCategory = primaryCategory;
        this.id = id;
    }
    
    @Override
    public String name() {
        return name;
    }

    @Override
    public int ordinal() {
        return id;
    }

    @Override
    public String primaryCategory() {
        return primaryCategory;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(name);
        sb.append(" Primary Category: ").append(primaryCategory);
        sb.append(" Id: ").append(id);
        
        return sb.toString();
    }
    
}
