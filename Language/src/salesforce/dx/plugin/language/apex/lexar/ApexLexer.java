/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.language.apex.lexar;

import salesforce.dx.plugin.language.apex.cls.lexer.*;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import salesforce.dx.plugin.language.apex.JavaCharStream;
import salesforce.dx.plugin.language.apex.JavaParserTokenManager;
import salesforce.dx.plugin.language.apex.Token;

/**
 *
 * @author seanjustice
 */
public class ApexLexer implements Lexer<ApexTokenId>{

    private LexerRestartInfo<ApexTokenId> info;
    private JavaParserTokenManager javaParserTokenManager;

    ApexLexer(LexerRestartInfo<ApexTokenId> info) {
        this.info = info;
        JavaCharStream stream = new JavaCharStream(info.input());
        javaParserTokenManager = new JavaParserTokenManager(stream);
    }

    @Override
    public org.netbeans.api.lexer.Token<ApexTokenId> nextToken() {
        Token token = javaParserTokenManager.getNextToken();
        if (info.input().readLength() < 1) {
            return null;
        }
        
        ApexTokenId tokenId = ApexLanguageHierarchy.getToken(token.kind);
        
        if(tokenId == null){
            return null;
        }
        
        return info.tokenFactory().createToken(tokenId);
    }

    @Override
    public Object state() {
        return null;
    }

    @Override
    public void release() {
    }
    
}
