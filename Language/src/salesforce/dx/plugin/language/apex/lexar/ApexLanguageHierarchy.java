/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.language.apex.lexar;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

/**
 *
 * @author seanjustice
 */
public abstract class ApexLanguageHierarchy extends LanguageHierarchy<ApexTokenId>{

    private static List<ApexTokenId> tokens;
    private static Map<Integer, ApexTokenId> idToToken;

    private static void init() {
        tokens = Arrays.<ApexTokenId>asList(new ApexTokenId[]{
            new ApexTokenId("EOF", "whitespace", 0),
            new ApexTokenId("WHITESPACE", "whitespace", 1),
            new ApexTokenId("SINGLE_LINE_COMMENT", "comment", 4),
            new ApexTokenId("FORMAL_COMMENT", "comment", 5),
            new ApexTokenId("MULTI_LINE_COMMENT", "comment", 6),
            new ApexTokenId("ABSTRACT", "keyword", 8),
            new ApexTokenId("ACTIVATE", "keyword", 9),
            new ApexTokenId("AND", "keyword", 10),
            new ApexTokenId("ANY", "keyword", 11),
            new ApexTokenId("AS", "keyword", 13),
            new ApexTokenId("ASC", "keyword", 14),
            new ApexTokenId("BEGIN", "keyword", 16),
            new ApexTokenId("BLOB", "keyword", 18),
            new ApexTokenId("BREAK", "keyword", 19),
            new ApexTokenId("BY", "keyword", 21),
            new ApexTokenId("CASE", "keyword", 23),
            new ApexTokenId("CATCH", "keyword", 25),
            new ApexTokenId("CHAR", "keyword", 26),
            new ApexTokenId("CLASS", "keyword", 27),
            new ApexTokenId("COLLECT", "keyword", 28),
            new ApexTokenId("COMMIT", "keyword", 29),
            new ApexTokenId("CONST", "keyword", 30),
            new ApexTokenId("CONTINUE", "keyword", 31),
            new ApexTokenId("CONVERTCURRENCY", "keyword", 32),
            new ApexTokenId("_DEFAULT", "keyword", 34),
            new ApexTokenId("DELETE", "keyword", 35),
            new ApexTokenId("DESC", "keyword", 36),
            new ApexTokenId("DO", "keyword", 37),
            new ApexTokenId("ELSE", "keyword", 38),
            new ApexTokenId("END", "keyword", 39),
            new ApexTokenId("ENUM", "keyword", 40),
            new ApexTokenId("EXTENDS", "keyword", 41),
            new ApexTokenId("EXCEPTION", "reserved", 42),
            new ApexTokenId("FALSE", "keyword", 45),
            new ApexTokenId("FINAL", "keyword", 46),
            new ApexTokenId("FINALLY", "keyword", 47),
            new ApexTokenId("FLOAT", "keyword", 48),
            new ApexTokenId("FOR", "keyword", 49),
            new ApexTokenId("FROM", "keyword", 50),
            new ApexTokenId("GLOBAL", "keyword", 51),
            new ApexTokenId("GOTO", "keyword", 52),
            new ApexTokenId("IF", "keyword", 56),
            new ApexTokenId("IMPLEMENTS", "keyword", 57),
            new ApexTokenId("IMPORT", "keyword", 58),
            new ApexTokenId("IN", "keyword", 59),
            new ApexTokenId("INSERT", "keyword", 61),
            new ApexTokenId("INSTANCEOF", "keyword", 62),
            new ApexTokenId("INTERFACE", "keyword", 63),
            new ApexTokenId("INT", "keyword", 65),
            new ApexTokenId("JOIN", "keyword", 66),
            new ApexTokenId("LAST_90_DAYS", "keyword", 67),
            new ApexTokenId("LAST_MONTH", "keyword", 68),
            new ApexTokenId("LAST_N_DAYS", "keyword", 69),
            new ApexTokenId("LAST_WEEK", "keyword", 70),
            new ApexTokenId("LIKE", "keyword", 71),
            new ApexTokenId("LIMIT", "keyword", 72),
            new ApexTokenId("LONG", "keyword", 74),
            new ApexTokenId("MERGE", "keyword", 77),
            new ApexTokenId("NEW", "keyword", 78),
            new ApexTokenId("NEXT_90_DAYS", "keyword", 79),
            new ApexTokenId("NEXT_MONTH", "keyword", 80),
            new ApexTokenId("NEXT_N_DAYS", "keyword", 81),
            new ApexTokenId("NEXT_WEEK", "keyword", 82),
            new ApexTokenId("NOT", "keyword", 83),
            new ApexTokenId("NULL", "keyword", 84),
            new ApexTokenId("NULLS", "keyword", 85),
            new ApexTokenId("OBJECT", "reserved", 87),
            new ApexTokenId("OF", "keyword", 88),
            new ApexTokenId("ON", "keyword", 89),
            new ApexTokenId("OR", "keyword", 90),
            new ApexTokenId("OVERRIDE", "keyword", 92),
            new ApexTokenId("PACKAGE", "keyword", 93),
            new ApexTokenId("PRIVATE", "keyword", 96),
            new ApexTokenId("PROTECTED", "keyword", 97),
            new ApexTokenId("PUBLIC", "keyword", 98),
            new ApexTokenId("RETURN", "keyword", 100),
            new ApexTokenId("SELECT", "keyword", 105),
            new ApexTokenId("SET", "reserved", 106),
            new ApexTokenId("STATIC", "keyword", 110),
            new ApexTokenId("STRICTFP", "keyword", 111),
            new ApexTokenId("SUPER", "keyword", 112),
            new ApexTokenId("SWITCH", "keyword", 113),
            new ApexTokenId("SYNCHRONIZED", "keyword", 114),
            new ApexTokenId("TESTMETHOD", "keyword", 116),
            new ApexTokenId("THEN", "keyword", 117),
            new ApexTokenId("THIS", "keyword", 118),
            new ApexTokenId("THIS_MONTH", "keyword", 119),
            new ApexTokenId("THIS_WEEK", "keyword", 120),
            new ApexTokenId("THROW", "keyword", 121),
            new ApexTokenId("TODAY", "keyword", 122),
            new ApexTokenId("TRANSACTION", "keyword", 125),
            new ApexTokenId("TRIGGER", "keyword", 126),
            new ApexTokenId("TRUE", "keyword", 127),
            new ApexTokenId("TRY", "keyword", 128),
            new ApexTokenId("TYPE", "keyword", 129),
            new ApexTokenId("UNDELETE", "keyword", 130),
            new ApexTokenId("UPDATE", "keyword", 131),
            new ApexTokenId("UPSERT", "keyword", 132),
            new ApexTokenId("USING", "keyword", 133),
            new ApexTokenId("VIRTUAL", "keyword", 134),
            new ApexTokenId("WHEN", "keyword", 136),
            new ApexTokenId("WHERE", "keyword", 137),
            new ApexTokenId("VOID", "keyword", 139),
            new ApexTokenId("WHILE", "keyword", 140),
            new ApexTokenId("INTEGER_LITERAL", "literal", 141),
            new ApexTokenId("DECIMAL_LITERAL", "literal", 142),
            new ApexTokenId("HEX_LITERAL", "literal", 143),
            new ApexTokenId("OCTAL_LITERAL", "literal", 144),
            new ApexTokenId("FLOATING_POINT_LITERAL", "literal", 145),
            new ApexTokenId("DECIMAL_FLOATING_POINT_LITERAL", "literal", 146),
            new ApexTokenId("DECIMAL_EXPONENT", "number", 147),
            new ApexTokenId("HEXADECIMAL_FLOATING_POINT_LITERAL", "literal", 148),
            new ApexTokenId("HEXADECIMAL_EXPONENT", "number", 149),
            new ApexTokenId("CHARACTER_LITERAL", "literal", 150),
            new ApexTokenId("STRING_LITERAL", "literal", 151),
            new ApexTokenId("IDENTIFIER", "identifier", 152),
            new ApexTokenId("LETTER", "literal", 153),
            new ApexTokenId("PART_LETTER", "literal", 154),
            new ApexTokenId("LPAREN", "operator", 155),
            new ApexTokenId("RPAREN", "operator", 156),
            new ApexTokenId("LBRACE", "operator", 157),
            new ApexTokenId("RBRACE", "operator", 158),
            new ApexTokenId("LBRACKET", "operator", 159),
            new ApexTokenId("RBRACKET", "operator", 160),
            new ApexTokenId("SEMICOLON", "operator", 161),
            new ApexTokenId("COMMA", "operator", 162),
            new ApexTokenId("DOT", "operator", 163),
            new ApexTokenId("AT", "operator", 164),
            new ApexTokenId("ASSIGN", "operator", 165),
            new ApexTokenId("LT", "operator", 166),
            new ApexTokenId("BANG", "operator", 167),
            new ApexTokenId("TILDE", "operator", 168),
            new ApexTokenId("HOOK", "operator", 169),
            new ApexTokenId("COLON", "operator", 170),
            new ApexTokenId("EQ", "operator", 171),
            new ApexTokenId("LE", "operator", 172),
            new ApexTokenId("GE", "operator", 173),
            new ApexTokenId("NE", "operator", 174),
            new ApexTokenId("SC_OR", "operator", 175),
            new ApexTokenId("SC_AND", "operator", 176),
            new ApexTokenId("INCR", "operator", 177),
            new ApexTokenId("DECR", "operator", 178),
            new ApexTokenId("PLUS", "operator", 179),
            new ApexTokenId("MINUS", "operator", 180),
            new ApexTokenId("STAR", "operator", 181),
            new ApexTokenId("SLASH", "operator", 182),
            new ApexTokenId("BIT_AND", "operator", 183),
            new ApexTokenId("BIT_OR", "operator", 184),
            new ApexTokenId("XOR", "operator", 185),
            new ApexTokenId("REM", "operator", 186),
            new ApexTokenId("LSHIFT", "operator", 187),
            new ApexTokenId("PLUSASSIGN", "operator", 188),
            new ApexTokenId("MINUSASSIGN", "operator", 189),
            new ApexTokenId("STARASSIGN", "operator", 190),
            new ApexTokenId("SLASHASSIGN", "operator", 191),
            new ApexTokenId("ANDASSIGN", "operator", 192),
            new ApexTokenId("ORASSIGN", "operator", 193),
            new ApexTokenId("XORASSIGN", "operator", 194),
            new ApexTokenId("REMASSIGN", "operator", 195),
            new ApexTokenId("LSHIFTASSIGN", "operator", 196),
            new ApexTokenId("RSIGNEDSHIFTASSIGN", "operator", 197),
            new ApexTokenId("RUNSIGNEDSHIFTASSIGN", "operator", 198),
            new ApexTokenId("ELLIPSIS", "operator", 199)
        });
        
        idToToken = new HashMap<>();
        for (ApexTokenId token : tokens) {
            idToToken.put(token.ordinal(), token);
        }
    }

    static synchronized ApexTokenId getToken(int id) {
        if (idToToken == null) {
            init();
        }
        return idToToken.get(id);
    }

    @Override
    protected synchronized Collection<ApexTokenId> createTokenIds() {
        if (tokens == null) {
            init();
        }
        return tokens;
    }

    @Override
    protected synchronized Lexer<ApexTokenId> createLexer(LexerRestartInfo<ApexTokenId> info) {
        return new ApexLexer(info);
    }
}
