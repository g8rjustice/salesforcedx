/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.language.apex.trigger.lexar;

import salesforce.dx.plugin.language.apex.lexar.ApexLanguageHierarchy;

/**
 *
 * @author seanjustice
 */
public class TriggerLanguageHierarchy extends ApexLanguageHierarchy{

    @Override
    protected String mimeType() {
        return "text/x-trigger";
    }
    
}
