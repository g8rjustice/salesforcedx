/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.language;

import org.netbeans.api.lexer.Language;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;
import org.netbeans.modules.csl.spi.LanguageRegistration;
import salesforce.dx.plugin.language.apex.cls.lexer.ClsLanguageHierarchy;

/**
 *
 * @author seanjustice
 */
@LanguageRegistration(mimeType = "text/x-cls")
public class ClsLanguage extends DefaultLanguageConfig{

    @Override
    public Language getLexerLanguage() {
        return new ClsLanguageHierarchy().language();
    }

    @Override
    public String getDisplayName() {
        return "CLS";
    }
    
}
