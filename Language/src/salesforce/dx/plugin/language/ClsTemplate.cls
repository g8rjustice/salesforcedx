/**
 * SimpleApexExample comment for SimpleApexExample class.
 * @author Simple Joe Smith
 */
public class SimpleApexExample {

    public List<Account> method (Integer param) {
        List<Account> accounts = [
            SELECT
                Id,
                NAME
            FROM
                Account
            WHERE
                Name = 'Example'
            LIMIT 10
            ];

        return accounts;
    }// line comment

}