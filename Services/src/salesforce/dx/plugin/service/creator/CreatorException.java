/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.service.creator;

/**
 *
 * @author seanjustice
 */
public class CreatorException extends Exception {

    /**
     * Creates a new instance of <code>CreatorException</code> without detail
     * message.
     */
    public CreatorException() {
    }

    /**
     * Constructs an instance of <code>CreatorException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public CreatorException(String msg) {
        super(msg);
    }
    
    public CreatorException(Throwable throwable){
        super(throwable);
    }
}
