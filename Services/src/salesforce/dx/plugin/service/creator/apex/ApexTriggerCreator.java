/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.service.creator.apex;

import java.nio.file.Path;
import java.util.Set;
import salesforce.dx.plugin.service.creator.CreatorException;

/**
 *
 * @author seanjustice
 */
public interface ApexTriggerCreator {
    public enum Events{
        BEFORE_INSERT,
        AFTER_INSERT,
        BEFORE_UPDATE,
        AFTER_UPDATE,
        BEFORE_DELETE,
        AFTER_DELETE,
        AFTER_UNDELETE;
    }
    
    public void create(Path path, String name, String sObject, Set<Events> events) throws CreatorException;
}
