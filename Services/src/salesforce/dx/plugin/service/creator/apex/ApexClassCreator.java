/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.service.creator.apex;

import java.nio.file.Path;
import salesforce.dx.plugin.service.creator.CreatorException;

/**
 *
 * @author seanjustice
 */
public interface ApexClassCreator {
    
    public enum Type{
        CLASS,
        EXCEPTION,
        UNIT_TEST,
        INBOUND_EMAIL_SERVICE;
    }
    
    public void create(Path path, String name, Type type) throws CreatorException;
}
