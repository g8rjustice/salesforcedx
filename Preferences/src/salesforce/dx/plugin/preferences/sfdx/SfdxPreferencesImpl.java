/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.preferences.sfdx;

import org.openide.util.NbPreferences;
import salesforce.dx.plugin.preferences.SfdxPreferences;

/**
 *
 * @author seanjustice
 */
public class SfdxPreferencesImpl implements SfdxPreferences{
    
    private static final String CLI_PATH = "cliPath";
    private static SfdxPreferencesImpl instance;
    
    public static SfdxPreferences getInstance(){
        
        if(instance == null){
            
            synchronized(SfdxPreferencesImpl.class){
                if(instance == null){
                    instance = new SfdxPreferencesImpl();
                }
            }
        }
        
        return instance;
    }
    
    private SfdxPreferencesImpl() { }
    
    @Override
    public String getCliPath() {
        return NbPreferences.forModule(SfdxPreferencesImpl.class).get(CLI_PATH, "");
    }

    @Override
    public void setCliPath(String path) {
        NbPreferences.forModule(SfdxPreferencesImpl.class).put(CLI_PATH, path);
    }
    
}
