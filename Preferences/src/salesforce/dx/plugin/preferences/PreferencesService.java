/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.preferences;

import salesforce.dx.plugin.preferences.sfdx.SfdxPreferencesImpl;

/**
 *
 * @author seanjustice
 */
public final class PreferencesService {
    
    private PreferencesService(){
        throw new AssertionError("Should never get here...");
    }
    
    public static SfdxPreferences getSfdxPreferences(){
        return SfdxPreferencesImpl.getInstance();
    }
}
