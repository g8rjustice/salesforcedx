/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package salesforce.dx.plugin.preferences;

/**
 *
 * @author seanjustice
 */
public interface SfdxPreferences {
 
    public String getCliPath();
    public void setCliPath(String path);
}
